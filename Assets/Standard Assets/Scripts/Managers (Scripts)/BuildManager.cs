#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.IO;
using System.Diagnostics;

//[ExecuteInEditMode]
public class BuildManager : SingletonMonoBehaviour<BuildManager>
{
	public BuildAction[] buildActions;
	public bool updateServerDataFiles;
	static BuildPlayerOptions buildOptions;

	static string[] GetScenePathsInBuild ()
	{
		List<string> scenePathsInBuild = new List<string>();
		string scenePath = null;
		for (int i = 0; i < EditorBuildSettings.scenes.Length; i ++)
		{
			EditorBuildSettingsScene scene = EditorBuildSettings.scenes[i];
			if (scene.enabled)
				scenePathsInBuild.Add(scene.path);
		}
		return scenePathsInBuild.ToArray();
	}
	
	[MenuItem("Build/Make builds")]
	static void _Build ()
	{
		BuildManager.Instance.Build ();
	}

	void Build ()
	{
		for (int i = 0; i < buildActions.Length; i ++)
		{
			BuildAction buildAction = buildActions[i];
			if (buildAction.enabled)
				buildAction.Do ();
		}
	}
	
	[Serializable]
	public struct BuildAction
	{
		public string name;
		public bool enabled;
		public BuildTarget target;
		public bool autoSetBuildLocationPath;
		public string buildLocationPath;
		public BuildOptions[] options;
		public bool makeZip;
		
		public void Do ()
		{
			if (autoSetBuildLocationPath)
				buildLocationPath = Application.dataPath + "/../Builds/" + Application.productName + " (" + name + ")";
			buildOptions = new BuildPlayerOptions();
			buildOptions.scenes = GetScenePathsInBuild();
			buildOptions.target = target;
			buildOptions.locationPathName = buildLocationPath;
			foreach (BuildOptions option in options)
				buildOptions.options |= option;
			BuildPipeline.BuildPlayer(buildOptions);
			if (makeZip)
			{
				string zipLocationPath = buildLocationPath + ".zip";
				if (File.Exists(zipLocationPath))
					File.Delete(zipLocationPath);
				DirectoryCompressionOperations.CompressDirectory (buildLocationPath, zipLocationPath);
			}
		}
	}
}
#endif