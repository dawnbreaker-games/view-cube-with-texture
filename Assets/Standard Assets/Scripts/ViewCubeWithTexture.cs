using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;
using static ViewCubeWithTexture;

class ViewCubeWithTexture : MonoBehaviour
{
	const int defaultFontSize = 14;
	public const float minCameraDistance = 1.5f;
	public const float maxCameraDistance = 3;
	public static InputField texturePathInputField;
	public static MeshRenderer meshRenderer;
	public static Transform cameraTrs;
	TextureUpdater textureUpdater;
	CameraOrientationUpdater cameraOrientationUpdater;
	IUpdatable[] updatables = new IUpdatable[0];

	void Start ()
	{
		GameObject cubeGo = GameObject.CreatePrimitive(PrimitiveType.Cube);
		meshRenderer = cubeGo.GetComponent<MeshRenderer>();
		meshRenderer.material.ChangeMaterialTransparency (true);
		GameObject canvasGo = new GameObject();
		Canvas canvas = canvasGo.AddComponent<Canvas>();
		canvasGo.AddComponent<GraphicRaycaster>();
		canvas.renderMode = RenderMode.ScreenSpaceOverlay;
		CanvasScaler canvasScaler = canvasGo.AddComponent<CanvasScaler>();
		canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
		canvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);
		canvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
		Transform canvasTrs = canvas.transform;
		GameObject texturePathInputFieldGo = new GameObject();
		RectTransform texturePathInputFieldRectTrs = texturePathInputFieldGo.AddComponent<RectTransform>();
		texturePathInputFieldRectTrs.SetParent(canvasTrs);
		texturePathInputFieldRectTrs.position += new Vector3(50, 50);
		texturePathInputField = texturePathInputFieldGo.AddComponent<InputField>();
		Image texturePathInputFieldImage = texturePathInputFieldGo.AddComponent<Image>();
		texturePathInputField.image = texturePathInputFieldImage;
		GameObject texturePathInputFieldTextGo = new GameObject();
		Text texturePathInputFieldText = texturePathInputFieldTextGo.AddComponent<Text>();
		texturePathInputFieldText.text = "Enter image path...";
		texturePathInputFieldText.color = Color.black;
		Font font = Font.CreateDynamicFontFromOSFont(Font.GetOSInstalledFontNames()[0], defaultFontSize);
		texturePathInputFieldText.font = font;
		Transform texturePathInputFieldTextTrs = texturePathInputFieldText.transform;
		texturePathInputFieldTextTrs.SetParent(texturePathInputFieldRectTrs);
		texturePathInputFieldTextTrs.position += new Vector3(50, 50);
		texturePathInputField.textComponent = texturePathInputFieldText;
		GameObject buttonGo = new GameObject();
		RectTransform buttonRectTrs = buttonGo.AddComponent<RectTransform>();
		buttonRectTrs.SetParent(canvasTrs);
		buttonRectTrs.position += new Vector3(200, 50);
		Button button = buttonGo.AddComponent<Button>();
		GameObject buttonTextGo = new GameObject();
		Text buttonText = buttonTextGo.AddComponent<Text>();
		Transform buttonTextTrs = buttonText.transform;
		buttonTextTrs.SetParent(buttonRectTrs);
		buttonTextTrs.position = buttonRectTrs.position;
		buttonText.color = Color.black;
		buttonText.text = "Update";
		buttonText.font = font;
		Image buttonImage = buttonGo.AddComponent<Image>();
		button.image = buttonImage;
		textureUpdater = new TextureUpdater();
		button.onClick.AddListener(textureUpdater.DoUpdate);
		GameObject autoUpdateTextureToggleGo = new GameObject();
		Toggle autoUpdateTextureToggle = autoUpdateTextureToggleGo.AddComponent<Toggle>();
		autoUpdateTextureToggle.onValueChanged.AddListener(SetAutoUpdateTexture);
		Transform autoUpdateTextureToggleTrs = autoUpdateTextureToggleGo.transform;
		autoUpdateTextureToggleTrs.SetParent(canvasTrs);
		autoUpdateTextureToggleTrs.position += new Vector3(350, 50);
		Image autoUpdateTextureToggleImage = autoUpdateTextureToggleGo.AddComponent<Image>();
		autoUpdateTextureToggle.image = autoUpdateTextureToggleImage;
		GameObject autoUpdateTextureToggleTextGo = new GameObject();
		Text autoUpdateTextureToggleText = autoUpdateTextureToggleTextGo.AddComponent<Text>();
		Transform autoUpdateTextureToggleTextTrs = autoUpdateTextureToggleText.transform;
		autoUpdateTextureToggleTextTrs.SetParent(autoUpdateTextureToggleTrs);
		autoUpdateTextureToggleTextTrs.position = autoUpdateTextureToggleTrs.position;
		autoUpdateTextureToggleText.color = Color.black;
		autoUpdateTextureToggleText.text = "X";
		autoUpdateTextureToggleText.font = font;
		autoUpdateTextureToggle.graphic = autoUpdateTextureToggleText;
		GameObject autoUpdateTextureToggleLabelGo = new GameObject();
		Text autoUpdateTextureToggleLabelText = autoUpdateTextureToggleLabelGo.AddComponent<Text>();
		Transform autoUpdateTextureToggleLabelTrs = autoUpdateTextureToggleLabelText.transform;
		autoUpdateTextureToggleLabelTrs.SetParent(canvasTrs);
		autoUpdateTextureToggleLabelTrs.position = autoUpdateTextureToggleTrs.position + new Vector3(0, 20);
		autoUpdateTextureToggleLabelTrs.SetSiblingIndex(canvasTrs.childCount - 2);
		autoUpdateTextureToggleLabelText.color = Color.black;
		autoUpdateTextureToggleLabelText.text = "Auto Update:";
		autoUpdateTextureToggleLabelText.font = font;
		GameObject eventSystemGo = new GameObject();
		eventSystemGo.AddComponent<StandaloneInputModule>();
		Camera camera = new GameObject().AddComponent<Camera>();
		cameraTrs = camera.transform;
		cameraTrs.position = Vector3.back * 2;
		cameraOrientationUpdater = new CameraOrientationUpdater();
		updatables = updatables.Add(cameraOrientationUpdater);
	}

	void SetAutoUpdateTexture (bool autoUpdate)
	{
		if (autoUpdate)
			updatables = updatables.Add(textureUpdater);
		else
			updatables = updatables.Remove(textureUpdater);
	}

	void Update ()
	{
		for (int i = 0; i < updatables.Length; i ++)
		{
			IUpdatable updatable = updatables[i];
			updatable.DoUpdate ();
		}
	}
}

class TextureUpdater : IUpdatable
{
	public void DoUpdate ()
	{
		if (!File.Exists(texturePathInputField.text))
			return;
		Texture2D texture = new Texture2D(0, 0);
		texture.LoadImage(File.ReadAllBytes(texturePathInputField.text));
		meshRenderer.sharedMaterial.mainTexture = texture;
	}
}

class CameraOrientationUpdater : IUpdatable
{
	const float rotateSensitivity = 300f;
	const float zoomSensitivity = 100f;

	public void DoUpdate ()
	{
		Vector2 mouseMovement = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")) * Time.deltaTime * rotateSensitivity;
		cameraTrs.RotateAround(Vector3.zero, cameraTrs.right, mouseMovement.y);
		cameraTrs.RotateAround(Vector3.zero, Vector3.up, mouseMovement.x);
		float cameraDistance = cameraTrs.position.magnitude + Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * zoomSensitivity;
		cameraDistance = Mathf.Clamp(cameraDistance, minCameraDistance, maxCameraDistance);
		cameraTrs.position = cameraTrs.position.normalized * cameraDistance;
	}
}

interface IUpdatable
{
	void DoUpdate ();
}

static class CollectionExtensions
{
	public static T[] Add<T> (this T[] array, T values)
	{
		List<T> _array = new List<T>(array);
		_array.Add(values);
		return _array.ToArray();
	}

	public static T[] Remove<T> (this T[] array, T values)
	{
		List<T> _array = new List<T>(array);
		_array.Remove(values);
		return _array.ToArray();
	}
}
static class MaterialExtensions
{
	public enum SurfaceType
	{
		Opaque,
		Transparent
	}

	public enum BlendMode
	{
		Alpha,
		Premultiply,
		Additive,
		Multiply
	}

	public static void ChangeMaterialTransparency (this Material material, bool shouldMakeTransparent)
	{
		if (shouldMakeTransparent)
		{
			material.SetFloat("_Surface", (float) SurfaceType.Transparent);
			material.SetFloat("_Blend", (float) BlendMode.Alpha);
		}
		else
			material.SetFloat("_Surface", (float) SurfaceType.Opaque);
		SetupMaterialBlendMode (material);
	}

	public static void SetupMaterialBlendMode (this Material material)
	{
		if (material == null)
			throw new ArgumentNullException("material");
		// bool alphaClip = material.GetFloat("_AlphaClip") == 1;
		// if (alphaClip)
		// 	material.EnableKeyword("_ALPHATEST_ON");
		// else
		// 	material.DisableKeyword("_ALPHATEST_ON");
		SurfaceType surfaceType = (SurfaceType) material.GetFloat("_Surface");
		if (surfaceType == 0)
		{
			material.SetOverrideTag("RenderType", "");
			material.SetInt("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.One);
			material.SetInt("_DstBlend", (int) UnityEngine.Rendering.BlendMode.Zero);
			material.SetInt("_ZWrite", 1);
			material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
			material.renderQueue = -1;
			material.SetShaderPassEnabled("ShadowCaster", true);
		}
		else
		{
			BlendMode blendMode = (BlendMode)material.GetFloat("_Blend");
			switch (blendMode)
			{
				case BlendMode.Alpha:
					material.SetOverrideTag("RenderType", "Transparent");
					material.SetInt("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.SrcAlpha);
					material.SetInt("_DstBlend", (int) UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
					material.SetInt("_ZWrite", 0);
					material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
					material.renderQueue = (int) UnityEngine.Rendering.RenderQueue.Transparent;
					material.SetShaderPassEnabled("ShadowCaster", false);
					break;
				case BlendMode.Premultiply:
					material.SetOverrideTag("RenderType", "Transparent");
					material.SetInt("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.One);
					material.SetInt("_DstBlend", (int) UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
					material.SetInt("_ZWrite", 0);
					material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
					material.renderQueue = (int) UnityEngine.Rendering.RenderQueue.Transparent;
					material.SetShaderPassEnabled("ShadowCaster", false);
					break;
				case BlendMode.Additive:
					material.SetOverrideTag("RenderType", "Transparent");
					material.SetInt("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.One);
					material.SetInt("_DstBlend", (int) UnityEngine.Rendering.BlendMode.One);
					material.SetInt("_ZWrite", 0);
					material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
					material.renderQueue = (int) UnityEngine.Rendering.RenderQueue.Transparent;
					material.SetShaderPassEnabled("ShadowCaster", false);
					break;
				case BlendMode.Multiply:
					material.SetOverrideTag("RenderType", "Transparent");
					material.SetInt("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.DstColor);
					material.SetInt("_DstBlend", (int) UnityEngine.Rendering.BlendMode.Zero);
					material.SetInt("_ZWrite", 0);
					material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
					material.renderQueue = (int) UnityEngine.Rendering.RenderQueue.Transparent;
					material.SetShaderPassEnabled("ShadowCaster", false);
					break;
			}
		}
	}
}